<?php

namespace Inertia\Framework;

use Illuminate\Support\ServiceProvider;

class InertiaServiceProvider extends ServiceProvider
{
    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
