<?php

namespace Inertia\Framework\Support\Facades;

use Illuminate\Support\Facades\Facade;

class Tab extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'tab';
    }
}
