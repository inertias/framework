<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Inertia\Framework\Database\Models\AdminUser;
use Inertia\Framework\Database\Models\Role;

$factory->define(AdminUser::class, function (Faker $faker) {
    $role = factory(Role::class)->create();

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => 'secret',
        'role_id' => $role->id,
        'is_super_admin' => rand(0, 1),
        'image_path' => null
    ];
});
