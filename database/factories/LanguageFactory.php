<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Inertia\Framework\Database\Models\Language;

$factory->define(Language::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'code' => $faker->languageCode
    ];
});
