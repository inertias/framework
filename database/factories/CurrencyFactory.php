<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Inertia\Framework\Database\Models\Currency;

$factory->define(Currency::class, function (Faker $faker) {
    return [
        'name' => 'US Dollar',
        'code' => $faker->currencyCode,
        'symbol' => '$',
        'conversion_rate' => $faker->randomFloat,
        'status' => 'ENABLED'
    ];
});
